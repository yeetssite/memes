# Basically, memes.

This is the place where all my memes will go. It's a part of my [site](https://yeetssite.github.io), and how it works is some github pages magic (shown below):

1. create a new repository of any name
2. go to settings
3. go to github pages
4. under "branches and deployment", deploy from the main branch
5. create an `index.html` and your page's cotent (cotent patent pending).
6. your page will be published at [yoursiteaddress] followed by "/[reponame]/"

> replace "[reponame]" with the name of your repository, and [yoursiteaddress] with the address of your website, ex.:
> https://yoursite.github.io/something/


*[in professors voice]* zeez eez an photographic example of what an url could look like if you had uploaded to your github pages repo vs. uploading to a new/other repo:

**upload to pages repo *(includes `.html` file extension)***

<img src="https://cdn.discordapp.com/attachments/1226709870238240880/1227734739423854726/Screenshot_20240410_153858_Samsung_Internet.png?ex=66297c14&is=66170714&hm=475e0e27f5c1606fd4ba56284f1a81dcb6ec4efaed135ad705d6a829ac257cca&" height="32" width="445">

**upload to new/seperate repo *(no `.html` file extension)***

<img src="https://media.discordapp.net/attachments/1226709870238240880/1227734739188977714/Screenshot_20240410_153547_Samsung_Internet.png?ex=66297c14&is=66170714&hm=308646f965e0890e6db4dde3c64890821860861232fadcd00d3486b38d5fc653&=&width=753&height=82" height="32" width="320">
